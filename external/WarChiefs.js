// ==UserScript==
// @name            WarChiefs - Tiberium Alliances Upgrade Base/Defense/Army
// @description     Upgrade your Base,Defense Army to a specific Level.
// @author          Eistee
// @version         13.07.07
// @namespace       http*://*.alliances.commandandconquer.com/*
// @include         http*://*.alliances.commandandconquer.com/*
// @require         http://usocheckup.redirectme.net/167564.js
// @icon            http://s3.amazonaws.com/uso_ss/icon/167564/large.png
// @updateURL       https://userscripts.org/scripts/source/167564.meta.js
// @downloadURL     https://userscripts.org/scripts/source/167564.user.js
// @grant           GM_getValue
// @grant           GM_log
// @grant           GM_openInTab
// @grant           GM_registerMenuCommand
// @grant           GM_setValue
// @grant           GM_xmlhttpRequest
// ==/UserScript==
(function () {
    var injectFunction = function () {
        function createClasses() {
            qx.Class.define("Upgrade", {
                type: "singleton",
                extend: qx.core.Object,
                construct: function () {
                    try {
                        var qxApp = qx.core.Init.getApplication();

                        this.btnUpgrade = new qx.ui.form.Button(qxApp.tr("tnf:toggle upgrade mode"), "FactionUI/icons/icon_building_detail_upgrade.png").set({
                            toolTipText: qxApp.tr("tnf:toggle upgrade mode"),
                            alignY: "middle",
                            show: "icon",
                            width : 60,
                            allowGrowX : false,
                            allowGrowY : false,
                            appearance : "button"
                        });
                        this.btnClick = this.btnUpgrade.addListener("click", this.openBase, this);

                        var playArea = qx.core.Init.getApplication().getPlayArea();
                        var btnTrade = playArea.getHUD().getUIItem(ClientLib.Data.Missions.PATH.WDG_TRADE);
                        btnTrade.getLayoutParent().addAfter(this.btnUpgrade, btnTrade);

                        phe.cnc.Util.attachNetEvent(ClientLib.Vis.VisMain.GetInstance(), "ViewModeChange", ClientLib.Vis.ViewModeChange, this, this.onViewChanged);
                    } catch (e) {
                        console.log("Error setting up Upgrade Constructor: ");
                        console.log(e.toString());
                    }
                },
                destruct: function () {},
                members: {
                    btnUpgrade: null,
                    btnClick: null,
                    onViewChanged: function (oldMode, newMode) {
                        this.btnUpgrade.removeListenerById(this.btnClick);
                        Upgrade.Base.getInstance().close();
                        Upgrade.Defense.getInstance().close();
                        Upgrade.Army.getInstance().close();
                        switch (newMode) {
                            case ClientLib.Vis.Mode.City:
                                this.btnUpgrade.show();
                                this.btnClick = this.btnUpgrade.addListener("click", this.openBase, this);
                                break;
                            case ClientLib.Vis.Mode.DefenseSetup:
                                this.btnUpgrade.show();
                                this.btnClick = this.btnUpgrade.addListener("click", this.openDefense, this);
                                break;
                            case ClientLib.Vis.Mode.ArmySetup:
                                this.btnUpgrade.show();
                                this.btnClick = this.btnUpgrade.addListener("click", this.openArmy, this);
                                break;
                            default:
                                this.btnUpgrade.hide();
                                break;
                        }
                    },
                    openBase: function () {
                        if (Upgrade.Base.getInstance().isVisible()) Upgrade.Base.getInstance().close();
                        else Upgrade.Base.getInstance().open();
                    },
                    openDefense: function () {
                        if (Upgrade.Defense.getInstance().isVisible()) Upgrade.Defense.getInstance().close();
                        else Upgrade.Defense.getInstance().open();
                    },
                    openArmy: function () {
                        if (Upgrade.Army.getInstance().isVisible()) Upgrade.Army.getInstance().close();
                        else Upgrade.Army.getInstance().open();
                    }
                }
            });
            qx.Class.define("Upgrade.Base", {
                type: "singleton",
                extend: qx.ui.window.Window,
                construct: function () {
                    try {
                        var qxApp = qx.core.Init.getApplication();
                        this.base(arguments);
                        this.set({
                            layout: new qx.ui.layout.VBox().set({ spacing: 0 }),
                            caption: qxApp.tr("tnf:toggle upgrade mode") + ": " + qxApp.tr("tnf:base"),
                            icon: "FactionUI/icons/icon_arsnl_base_buildings.png",
                            contentPadding: 5,
                            contentPaddingTop: 0,
                            allowMaximize: false,
                            showMaximize: false,
                            allowMinimize: false,
                            showMinimize: false,
                            resizable: false
                        });
                        this.moveTo(124, 31);
                        this.getChildControl("icon").set({ width : 18, height : 18, scale : true, alignY : "middle" });

                        var cntCurrent = new qx.ui.container.Composite(new qx.ui.layout.VBox(5)).set({ padding: 5, decorator: "pane-light-opaque" });
                        cntCurrent.add(new qx.ui.basic.Label(qxApp.tr("Upgrade current building")).set({ alignX: "center", font: "font_size_14_bold" }));
                        this.lblCurrent = new qx.ui.basic.Label(qxApp.tr("Selected building:") + " -");
                        cntCurrent.add(this.lblCurrent);
                        var cntCurrentHBox = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
                        cntCurrentHBox.add(new qx.ui.basic.Label(qxApp.tr("tnf:level:")).set({ alignY: "middle" }));
                        this.txtCurrentNewLevel = new qx.ui.form.Spinner(1).set({ maximum: 150, minimum: 1 });
                        this.txtCurrentNewLevel.addListener("changeValue", this.onCurrentNewLevelInput, this);
                        cntCurrentHBox.add(this.txtCurrentNewLevel);
                        var btnCurrent = new qx.ui.form.Button(qxApp.tr("tnf:toggle upgrade mode"), "FactionUI/icons/icon_building_detail_upgrade.png");
                        btnCurrent.addListener("execute", this.upgradeCurrent, this);
                        cntCurrentHBox.add(btnCurrent);
                        cntCurrent.add(cntCurrentHBox);
                        var cntCurrentRes = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
                        cntCurrentRes.add(new qx.ui.basic.Label(qxApp.tr("tnf:requires:")));
                        cntCurrentRes.add(this.lblCurrentTib = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_tiberium.png"));
                        this.lblCurrentTib.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntCurrentRes.add(this.lblCurrentPow = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_power.png"));
                        this.lblCurrentPow.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntCurrent.add(cntCurrentRes);
                        this.add(cntCurrent);

                        var cntAll = new qx.ui.container.Composite(new qx.ui.layout.VBox(5)).set({ padding: 5, decorator: "pane-light-opaque" });
                        cntAll.add(new qx.ui.basic.Label(qxApp.tr("Upgrade all buildings")).set({ alignX: "center", font: "font_size_14_bold" }));
                        var cntAllHBox = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
                        cntAllHBox.add(new qx.ui.basic.Label(qxApp.tr("tnf:level:")).set({ alignY: "middle" }));
                        this.txtAllNewLevel = new qx.ui.form.Spinner(1).set({ maximum: 150, minimum: 1 });
                        this.txtAllNewLevel.addListener("changeValue", this.onAllNewLevelInput, this);
                        cntAllHBox.add(this.txtAllNewLevel);
                        var btnAll = new qx.ui.form.Button(qxApp.tr("tnf:toggle upgrade mode"), "FactionUI/icons/icon_building_detail_upgrade.png");
                        btnAll.addListener("execute", this.upgradeAll, this);
                        cntAllHBox.add(btnAll);
                        cntAll.add(cntAllHBox);
                        var cntAllRes = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
                        cntAllRes.add(new qx.ui.basic.Label(qxApp.tr("tnf:requires:")));
                        cntAllRes.add(this.lblAllTib = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_tiberium.png"));
                        this.lblAllTib.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntAllRes.add(this.lblAllPow = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_power.png"));
                        this.lblAllPow.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntAll.add(cntAllRes);
                        this.add(cntAll);


                        this.addListener("close", this.onClose, this);
                        this.addListener("appear", this.onAppear, this);
                        phe.cnc.Util.attachNetEvent(ClientLib.Vis.VisMain.GetInstance(), "SelectionChange", ClientLib.Vis.SelectionChange, this, this.onSelectionChange);
                    } catch (e) {
                        console.log("Error setting up Upgrade.Base Constructor: ");
                        console.log(e.toString());
                    }
                },
                destruct: function () {},
                members: {
                    lblCurrent: null,
                    lblCurrentTib: null,
                    lblCurrentPow: null,
                    txtCurrentNewLevel: null,
                    Current: null,
                    lblAllTib: null,
                    lblAllPow: null,
                    txtAllNewLevel: null,
                    onAppear: function () {
                        var allLowLevel = this.calcAllLowLevel();
                        this.txtAllNewLevel.setValue(allLowLevel);
                        this.txtAllNewLevel.setMinimum(allLowLevel);
                        this.onAllNewLevelInput();
                    },
                    onClose: function () {
                        var qxApp = qx.core.Init.getApplication();
                        this.lblCurrent.setValue(qxApp.tr("Selected building:") + " -");
                        this.txtCurrentNewLevel.setValue(1);
                        this.txtCurrentNewLevel.resetValue();
                        this.Current = null;
                        this.txtAllNewLevel.setMinimum(1);
                        this.txtAllNewLevel.resetValue();
                    },
                    onSelectionChange: function(oldObj, newObj) {
                        var qxApp = qx.core.Init.getApplication();
                        if (newObj != null && newObj.get_VisObjectType() == ClientLib.Vis.VisObject.EObjectType.CityBuildingType) {
                            this.Current = newObj;
                            var name = newObj.get_BuildingName();
                            var level = newObj.get_BuildingLevel();
                            this.lblCurrent.setValue(qxApp.tr("Selected building:") + " " + name + " (" + level + ")");
                            this.txtCurrentNewLevel.setMinimum(level + 1);
                            this.txtCurrentNewLevel.setValue(level + 1);
                            this.onCurrentNewLevelInput();
                        }
                    },
                    onCurrentNewLevelInput: function () {
                        var newLevel = parseInt(this.txtCurrentNewLevel.getValue(), 10);
                        if (newLevel > 0) {
                            var obj = this.Current;
                            if (obj !== null && obj.get_VisObjectType() == ClientLib.Vis.VisObject.EObjectType.CityBuildingType && newLevel > obj.get_BuildingLevel()) {
                                var costs = ClientLib.API.City.GetInstance().GetUpgradeCostsForBuildingToLevel(obj.get_BuildingDetails(), newLevel);
                                var Tib = 0;
                                var Pow = 0;
                                for (var i = 0; i < costs.length; i++) {
                                    var uCosts = costs[i];
                                    var cType = parseInt(uCosts.Type, 10);
                                    switch (cType) {
                                        case ClientLib.Base.EResourceType.Tiberium:
                                            Tib += uCosts.Count;
                                            break;
                                        case ClientLib.Base.EResourceType.Power:
                                            Pow += uCosts.Count;
                                            break;
                                    }
                                }
                                this.lblCurrentTib.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Tib));
                                this.lblCurrentTib.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Tib));
                                this.lblCurrentTib.setToolTipIcon("webfrontend/ui/common/icn_res_tiberium.png");
                                if (Tib === 0) this.lblCurrentTib.exclude();
                                else this.lblCurrentTib.show();
                                this.lblCurrentPow.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Pow));
                                this.lblCurrentPow.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Pow));
                                this.lblCurrentPow.setToolTipIcon("webfrontend/ui/common/icn_res_power.png");
                                if (Pow === 0) this.lblCurrentPow.exclude();
                                else this.lblCurrentPow.show();
                            } else {
                                this.lblCurrentTib.setLabel("-");
                                this.lblCurrentTib.resetToolTipText();
                                this.lblCurrentTib.resetToolTipIcon();
                                this.lblCurrentTib.show();
                                this.lblCurrentPow.setLabel("-");
                                this.lblCurrentPow.resetToolTipText();
                                this.lblCurrentPow.resetToolTipIcon();
                                this.lblCurrentPow.show();
                            }
                        } else {
                            this.lblCurrentTib.setLabel("-");
                            this.lblCurrentTib.resetToolTipText();
                            this.lblCurrentTib.resetToolTipIcon();
                            this.lblCurrentTib.show();
                            this.lblCurrentPow.setLabel("-");
                            this.lblCurrentPow.resetToolTipText();
                            this.lblCurrentPow.resetToolTipIcon();
                            this.lblCurrentPow.show();
                        }
                    },
                    upgradeCurrent: function() {
                        var newLevel = parseInt(this.txtCurrentNewLevel.getValue(), 10);
                        if (newLevel > 0) {
                            var obj = this.Current;
                            if (obj !== null && obj.get_VisObjectType() == ClientLib.Vis.VisObject.EObjectType.CityBuildingType && newLevel > obj.get_BuildingLevel()) {
                                ClientLib.API.City.GetInstance().UpgradeBuildingToLevel(obj.get_BuildingDetails(), newLevel);
                                this.onSelectionChange(null, this.Current);
                            }
                        }
                        this.txtCurrentNewLevel.setValue("");
                    },
                    calcAllLowLevel: function () {
                        for (var newLevel = 1, Tib = 0, Pow = 0; Tib === 0 && Pow === 0; newLevel++) {
                            var costs = ClientLib.API.City.GetInstance().GetUpgradeCostsForAllBuildingsToLevel(newLevel);
                            if (costs !== null) {
                                for (var i = 0; i < costs.length; i++) {
                                    var uCosts = costs[i];
                                    var cType = parseInt(uCosts.Type, 10);
                                    switch (cType) {
                                        case ClientLib.Base.EResourceType.Tiberium:
                                            Tib += uCosts.Count;
                                            break;
                                        case ClientLib.Base.EResourceType.Power:
                                            Pow += uCosts.Count;
                                            break;
                                    }
                                }
                            }
                        }
                        return (newLevel - 1);
                    },
                    onAllNewLevelInput: function () {
                        var newLevel = parseInt(this.txtAllNewLevel.getValue(), 10);
                        var costs = ClientLib.API.City.GetInstance().GetUpgradeCostsForAllBuildingsToLevel(newLevel);
                        if (newLevel > 0 && costs !== null) {
                            var Tib = 0;
                            var Pow = 0;
                            for (var i = 0; i < costs.length; i++) {
                                var uCosts = costs[i];
                                var cType = parseInt(uCosts.Type, 10);
                                switch (cType) {
                                    case ClientLib.Base.EResourceType.Tiberium:
                                        Tib += uCosts.Count;
                                        break;
                                    case ClientLib.Base.EResourceType.Power:
                                        Pow += uCosts.Count;
                                        break;
                                }
                            }
                            this.lblAllTib.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Tib));
                            this.lblAllTib.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Tib));
                            this.lblAllTib.setToolTipIcon("webfrontend/ui/common/icn_res_tiberium.png");
                            if (Tib === 0) this.lblAllTib.exclude();
                            else this.lblAllTib.show();
                            this.lblAllPow.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Pow));
                            this.lblAllPow.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Pow));
                            this.lblAllPow.setToolTipIcon("webfrontend/ui/common/icn_res_power.png");
                            if (Pow === 0) this.lblAllPow.exclude();
                            else this.lblAllPow.show();
                        } else {
                            this.lblAllTib.setLabel("-");
                            this.lblAllTib.resetToolTipText();
                            this.lblAllTib.resetToolTipIcon();
                            this.lblAllTib.show();
                            this.lblAllPow.setLabel("-");
                            this.lblAllPow.resetToolTipText();
                            this.lblAllPow.resetToolTipIcon();
                            this.lblAllPow.show();
                        }
                    },
                    upgradeAll: function() {
                        var newLevel = parseInt(this.txtAllNewLevel.getValue(), 10);
                        if (newLevel > 0) ClientLib.API.City.GetInstance().UpgradeAllBuildingsToLevel(newLevel);
                        this.txtAllNewLevel.setValue("");
                    }
                }
            });
            qx.Class.define("Upgrade.Defense", {
                type: "singleton",
                extend: qx.ui.window.Window,
                construct: function () {
                    try {
                        var qxApp = qx.core.Init.getApplication();
                        this.base(arguments);
                        this.set({
                            layout: new qx.ui.layout.VBox().set({ spacing: 0 }),
                            caption: qxApp.tr("tnf:toggle upgrade mode") + ": " + qxApp.tr("tnf:defense"),
                            icon: "FactionUI/icons/icon_def_army_points.png",
                            contentPadding: 5,
                            contentPaddingTop: 0,
                            allowMaximize: false,
                            showMaximize: false,
                            allowMinimize: false,
                            showMinimize: false,
                            resizable: false
                        });
                        this.moveTo(124, 31);
                        this.getChildControl("icon").set({ width : 18, height : 18, scale : true, alignY : "middle" });

                        var cntCurrent = new qx.ui.container.Composite(new qx.ui.layout.VBox(5)).set({ padding: 5, decorator: "pane-light-opaque" });
                        cntCurrent.add(new qx.ui.basic.Label(qxApp.tr("Upgrade current defense unit")).set({ alignX: "center", font: "font_size_14_bold" }));
                        this.lblCurrent = new qx.ui.basic.Label(qxApp.tr("Selected defense unit:") + " -");
                        cntCurrent.add(this.lblCurrent);
                        var cntCurrentHBox = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
                        cntCurrentHBox.add(new qx.ui.basic.Label(qxApp.tr("tnf:level:")).set({ alignY: "middle" }));
                        this.txtCurrentNewLevel = new qx.ui.form.Spinner(1).set({ maximum: 150, minimum: 1 });
                        this.txtCurrentNewLevel.addListener("changeValue", this.onCurrentNewLevelInput, this);
                        cntCurrentHBox.add(this.txtCurrentNewLevel);
                        var btnCurrent = new qx.ui.form.Button(qxApp.tr("tnf:toggle upgrade mode"), "FactionUI/icons/icon_building_detail_upgrade.png");
                        btnCurrent.addListener("execute", this.upgradeCurrent, this);
                        cntCurrentHBox.add(btnCurrent);
                        cntCurrent.add(cntCurrentHBox);
                        var cntCurrentRes = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
                        cntCurrentRes.add(new qx.ui.basic.Label(qxApp.tr("tnf:requires:")));
                        cntCurrentRes.add(this.lblCurrentTib = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_tiberium.png"));
                        this.lblCurrentTib.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntCurrentRes.add(this.lblCurrentCry = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_chrystal.png"));
                        this.lblCurrentCry.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntCurrentRes.add(this.lblCurrentPow = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_power.png"));
                        this.lblCurrentPow.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntCurrent.add(cntCurrentRes);
                        this.add(cntCurrent);

                        var cntAll = new qx.ui.container.Composite(new qx.ui.layout.VBox(5)).set({ padding: 5, decorator: "pane-light-opaque" });
                        cntAll.add(new qx.ui.basic.Label(qxApp.tr("Upgrade all defense units")).set({ alignX: "center", font: "font_size_14_bold" }));
                        var cntAllHBox = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
                        cntAllHBox.add(new qx.ui.basic.Label(qxApp.tr("tnf:level:")).set({ alignY: "middle" }));
                        this.txtAllNewLevel = new qx.ui.form.Spinner(1).set({ maximum: 150, minimum: 1 });
                        this.txtAllNewLevel.addListener("changeValue", this.onAllNewLevelInput, this);
                        cntAllHBox.add(this.txtAllNewLevel);
                        var btnAll = new qx.ui.form.Button(qxApp.tr("tnf:toggle upgrade mode"), "FactionUI/icons/icon_building_detail_upgrade.png");
                        btnAll.addListener("execute", this.upgradeAll, this);
                        cntAllHBox.add(btnAll);
                        cntAll.add(cntAllHBox);
                        var cntAllRes = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
                        cntAllRes.add(new qx.ui.basic.Label(qxApp.tr("tnf:requires:")));
                        cntAllRes.add(this.lblAllTib = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_tiberium.png"));
                        this.lblAllTib.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntAllRes.add(this.lblAllCry = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_chrystal.png"));
                        this.lblAllCry.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntAllRes.add(this.lblAllPow = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_power.png"));
                        this.lblAllPow.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntAll.add(cntAllRes);
                        this.add(cntAll);

                        this.addListener("close", this.onClose, this);
                        this.addListener("appear", this.onAppear, this);
                        phe.cnc.Util.attachNetEvent(ClientLib.Vis.VisMain.GetInstance(), "SelectionChange", ClientLib.Vis.SelectionChange, this, this.onSelectionChange);
                    } catch (e) {
                        console.log("Error setting up Upgrade.Defense Constructor: ");
                        console.log(e.toString());
                    }
                },
                destruct: function () {},
                members: {
                    lblCurrent: null,
                    lblCurrentTib: null,
                    lblCurrentCry: null,
                    lblCurrentPow: null,
                    txtCurrentNewLevel: null,
                    Current: null,
                    lblAllTib: null,
                    lblAllCry: null,
                    lblAllPow: null,
                    txtAllNewLevel: null,
                    onAppear: function () {
                        var allLowLevel = this.calcAllLowLevel();
                        this.txtAllNewLevel.setValue(allLowLevel);
                        this.txtAllNewLevel.setMinimum(allLowLevel);
                        this.onAllNewLevelInput();
                    },
                    onClose: function () {
                        var qxApp = qx.core.Init.getApplication();
                        this.lblCurrent.setValue(qxApp.tr("Selected defense unit:") + " -");
                        this.txtCurrentNewLevel.setValue(1);
                        this.txtCurrentNewLevel.resetValue();
                        this.Current = null;
                        this.txtAllNewLevel.setMinimum(1);
                        this.txtAllNewLevel.resetValue();
                    },
                    onSelectionChange: function(oldObj, newObj) {
                        var qxApp = qx.core.Init.getApplication();
                        if (newObj != null && newObj.get_VisObjectType() == ClientLib.Vis.VisObject.EObjectType.DefenseUnitType) {
                            this.Current = newObj;
                            var name = newObj.get_UnitName();
                            var level = newObj.get_UnitLevel();
                            this.lblCurrent.setValue(qxApp.tr("Selected defense unit:") + " " + name + " (" + level.toString() + ")");
                            this.txtCurrentNewLevel.setMinimum(level + 1);
                            this.txtCurrentNewLevel.setValue(level + 1);
                            this.onCurrentNewLevelInput();
                        }
                    },
                    onCurrentNewLevelInput: function () {
                        var newLevel = parseInt(this.txtCurrentNewLevel.getValue(), 10);
                        if (newLevel > 0) {
                            var obj = this.Current;
                            if (obj !== null && obj.get_VisObjectType() == ClientLib.Vis.VisObject.EObjectType.DefenseUnitType && newLevel > obj.get_UnitLevel()) {
                                var costs = ClientLib.API.Defense.GetInstance().GetUpgradeCostsForUnitToLevel(obj.get_UnitDetails(), newLevel);
                                var Tib = 0;
                                var Cry = 0;
                                var Pow = 0;
                                for (var i = 0; i < costs.length; i++) {
                                    var uCosts = costs[i];
                                    var cType = parseInt(uCosts.Type, 10);
                                    switch (cType) {
                                        case ClientLib.Base.EResourceType.Tiberium:
                                            Tib += uCosts.Count;
                                            break;
                                        case ClientLib.Base.EResourceType.Crystal:
                                            Cry += uCosts.Count;
                                            break;
                                        case ClientLib.Base.EResourceType.Power:
                                            Pow += uCosts.Count;
                                            break;
                                    }
                                }
                                this.lblCurrentTib.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Tib));
                                this.lblCurrentTib.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Tib));
                                this.lblCurrentTib.setToolTipIcon("webfrontend/ui/common/icn_res_tiberium.png");
                                if (Tib === 0) this.lblCurrentTib.exclude();
                                else this.lblCurrentTib.show();
                                this.lblCurrentCry.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Cry));
                                this.lblCurrentCry.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Cry));
                                this.lblCurrentCry.setToolTipIcon("webfrontend/ui/common/icn_res_chrystal.png");
                                if (Cry === 0) this.lblCurrentCry.exclude();
                                else this.lblCurrentCry.show();
                                this.lblCurrentPow.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Pow));
                                this.lblCurrentPow.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Pow));
                                this.lblCurrentPow.setToolTipIcon("webfrontend/ui/common/icn_res_power.png");
                                if (Pow === 0) this.lblCurrentPow.exclude();
                                else this.lblCurrentPow.show();
                            } else {
                                this.lblCurrentTib.setLabel("-");
                                this.lblCurrentTib.resetToolTipText();
                                this.lblCurrentTib.resetToolTipIcon();
                                this.lblCurrentTib.show();
                                this.lblCurrentCry.setLabel("-");
                                this.lblCurrentCry.resetToolTipText();
                                this.lblCurrentCry.resetToolTipIcon();
                                this.lblCurrentCry.show();
                                this.lblCurrentPow.setLabel("-");
                                this.lblCurrentPow.resetToolTipText();
                                this.lblCurrentPow.resetToolTipIcon();
                                this.lblCurrentPow.show();
                            }
                        } else {
                            this.lblCurrentTib.setLabel("-");
                            this.lblCurrentTib.resetToolTipText();
                            this.lblCurrentTib.resetToolTipIcon();
                            this.lblCurrentTib.show();
                            this.lblCurrentCry.setLabel("-");
                            this.lblCurrentCry.resetToolTipText();
                            this.lblCurrentCry.resetToolTipIcon();
                            this.lblCurrentCry.show();
                            this.lblCurrentPow.setLabel("-");
                            this.lblCurrentPow.resetToolTipText();
                            this.lblCurrentPow.resetToolTipIcon();
                            this.lblCurrentPow.show();
                        }
                    },
                    upgradeCurrent: function() {
                        var newLevel = parseInt(this.txtCurrentNewLevel.getValue(), 10);
                        if (newLevel > 0) {
                            var obj = this.Current;
                            if (obj !== null && obj.get_VisObjectType() == ClientLib.Vis.VisObject.EObjectType.DefenseUnitType)
                                ClientLib.API.Defense.GetInstance().UpgradeUnitToLevel(obj.get_UnitDetails(), newLevel);
                            this.onSelectionChange(null, this.Current);
                        }
                        this.txtCurrentNewLevel.setValue("");
                    },
                    calcAllLowLevel: function () {
                        for (var newLevel = 1, Tib = 0, Cry = 0, Pow = 0; Tib === 0 && Cry === 0 && Pow === 0; newLevel++) {
                            var costs = ClientLib.API.Defense.GetInstance().GetUpgradeCostsForAllUnitsToLevel(newLevel);
                            if (costs !== null) {
                                for (var i = 0; i < costs.length; i++) {
                                    var uCosts = costs[i];
                                    var cType = parseInt(uCosts.Type, 10);
                                    switch (cType) {
                                        case ClientLib.Base.EResourceType.Tiberium:
                                            Tib += uCosts.Count;
                                            break;
                                        case ClientLib.Base.EResourceType.Crystal:
                                            Cry += uCosts.Count;
                                            break;
                                        case ClientLib.Base.EResourceType.Power:
                                            Pow += uCosts.Count;
                                            break;
                                    }
                                }
                            }
                        }
                        return (newLevel - 1);
                    },
                    onAllNewLevelInput: function () {
                        var newLevel = parseInt(this.txtAllNewLevel.getValue(), 10);
                        var costs = ClientLib.API.Defense.GetInstance().GetUpgradeCostsForAllUnitsToLevel(newLevel);
                        if (newLevel > 0 && costs !== null) {
                            debug = costs;
                            var Tib = 0;
                            var Cry = 0;
                            var Pow = 0;
                            for (var i = 0; i < costs.length; i++) {
                                var uCosts = costs[i];
                                var cType = parseInt(uCosts.Type, 10);
                                switch (cType) {
                                    case ClientLib.Base.EResourceType.Tiberium:
                                        Tib += uCosts.Count;
                                        break;
                                    case ClientLib.Base.EResourceType.Crystal:
                                        Cry += uCosts.Count;
                                        break;
                                    case ClientLib.Base.EResourceType.Power:
                                        Pow += uCosts.Count;
                                        break;
                                }
                            }
                            this.lblAllTib.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Tib));
                            this.lblAllTib.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Tib));
                            this.lblAllTib.setToolTipIcon("webfrontend/ui/common/icn_res_tiberium.png");
                            if (Tib === 0) this.lblAllTib.exclude();
                            else this.lblAllTib.show();
                            this.lblAllCry.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Cry));
                            this.lblAllCry.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Cry));
                            this.lblAllCry.setToolTipIcon("webfrontend/ui/common/icn_res_chrystal.png");
                            if (Cry === 0) this.lblAllCry.exclude();
                            else this.lblAllCry.show();
                            this.lblAllPow.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Pow));
                            this.lblAllPow.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Pow));
                            this.lblAllPow.setToolTipIcon("webfrontend/ui/common/icn_res_power.png");
                            if (Pow === 0) this.lblAllPow.exclude();
                            else this.lblAllPow.show();
                        } else {
                            this.lblAllTib.setLabel("-");
                            this.lblAllTib.resetToolTipText();
                            this.lblAllTib.resetToolTipIcon();
                            this.lblAllTib.show();
                            this.lblAllCry.setLabel("-");
                            this.lblAllCry.resetToolTipText();
                            this.lblAllCry.resetToolTipIcon();
                            this.lblAllCry.show();
                            this.lblAllPow.setLabel("-");
                            this.lblAllPow.resetToolTipText();
                            this.lblAllPow.resetToolTipIcon();
                            this.lblAllPow.show();
                        }
                    },
                    upgradeAll: function() {
                        var newLevel = parseInt(this.txtAllNewLevel.getValue(), 10);
                        if (newLevel > 0) ClientLib.API.Defense.GetInstance().UpgradeAllUnitsToLevel(newLevel);
                        this.txtAllNewLevel.setValue("");
                    }
                }
            });
            qx.Class.define("Upgrade.Army", {
                type: "singleton",
                extend: qx.ui.window.Window,
                construct: function () {
                    try {
                        var qxApp = qx.core.Init.getApplication();
                        this.base(arguments);
                        this.set({
                            layout: new qx.ui.layout.VBox().set({ spacing: 0 }),
                            caption: qxApp.tr("tnf:toggle upgrade mode") + ": " + qxApp.tr("tnf:offense"),
                            icon: "FactionUI/icons/icon_army_points.png",
                            contentPadding: 5,
                            contentPaddingTop: 0,
                            allowMaximize: false,
                            showMaximize: false,
                            allowMinimize: false,
                            showMinimize: false,
                            resizable: false
                        });
                        this.moveTo(124, 31);
                        this.getChildControl("icon").set({ width : 18, height : 18, scale : true, alignY : "middle" });

                        var cntCurrent = new qx.ui.container.Composite(new qx.ui.layout.VBox(5)).set({ padding: 5, decorator: "pane-light-opaque" });
                        cntCurrent.add(new qx.ui.basic.Label(qxApp.tr("Upgrade current army unit")).set({ alignX: "center", font: "font_size_14_bold" }));
                        this.lblCurrent = new qx.ui.basic.Label(qxApp.tr("Selected army unit:") + " -");
                        cntCurrent.add(this.lblCurrent);
                        var cntCurrentHBox = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
                        cntCurrentHBox.add(new qx.ui.basic.Label(qxApp.tr("tnf:level:")).set({ alignY: "middle" }));
                        this.txtCurrentNewLevel = new qx.ui.form.Spinner(1).set({ maximum: 150, minimum: 1 });
                        this.txtCurrentNewLevel.addListener("changeValue", this.onCurrentNewLevelInput, this);
                        cntCurrentHBox.add(this.txtCurrentNewLevel);
                        var btnCurrent = new qx.ui.form.Button(qxApp.tr("tnf:toggle upgrade mode"), "FactionUI/icons/icon_building_detail_upgrade.png");
                        btnCurrent.addListener("execute", this.upgradeCurrent, this);
                        cntCurrentHBox.add(btnCurrent);
                        cntCurrent.add(cntCurrentHBox);
                        var cntCurrentRes = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
                        cntCurrentRes.add(new qx.ui.basic.Label(qxApp.tr("tnf:requires:")));
                        cntCurrentRes.add(this.lblCurrentCry = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_chrystal.png"));
                        this.lblCurrentCry.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntCurrentRes.add(this.lblCurrentPow = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_power.png"));
                        this.lblCurrentPow.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntCurrent.add(cntCurrentRes);
                        this.add(cntCurrent);

                        var cntAll = new qx.ui.container.Composite(new qx.ui.layout.VBox(5)).set({ padding: 5, decorator: "pane-light-opaque" });
                        cntAll.add(new qx.ui.basic.Label(qxApp.tr("Upgrade all army units")).set({ alignX: "center", font: "font_size_14_bold" }));
                        var cntAllHBox = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
                        cntAllHBox.add(new qx.ui.basic.Label(qxApp.tr("tnf:level:")).set({ alignY: "middle" }));
                        this.txtAllNewLevel = new qx.ui.form.Spinner(1).set({ maximum: 150, minimum: 1 });
                        this.txtAllNewLevel.addListener("changeValue", this.onAllNewLevelInput, this);
                        cntAllHBox.add(this.txtAllNewLevel);
                        var btnAll = new qx.ui.form.Button(qxApp.tr("tnf:toggle upgrade mode"), "FactionUI/icons/icon_building_detail_upgrade.png");
                        btnAll.addListener("execute", this.upgradeAll, this);
                        cntAllHBox.add(btnAll);
                        cntAll.add(cntAllHBox);
                        var cntAllRes = new qx.ui.container.Composite(new qx.ui.layout.HBox(5));
                        cntAllRes.add(new qx.ui.basic.Label(qxApp.tr("tnf:requires:")));
                        cntAllRes.add(this.lblAllCry = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_chrystal.png"));
                        this.lblAllCry.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntAllRes.add(this.lblAllPow = new qx.ui.basic.Atom("-", "webfrontend/ui/common/icn_res_power.png"));
                        this.lblAllPow.getChildControl("icon").set({width: 18, height: 18, scale: true, alignY : "middle"});
                        cntAll.add(cntAllRes);
                        this.add(cntAll);

                        this.addListener("close", this.onClose, this);
                        this.addListener("appear", this.onAppear, this);
                        phe.cnc.Util.attachNetEvent(ClientLib.Vis.VisMain.GetInstance(), "SelectionChange", ClientLib.Vis.SelectionChange, this, this.onSelectionChange);
                    } catch (e) {
                        console.log("Error setting up Upgrade.Army Constructor: ");
                        console.log(e.toString());
                    }
                },
                destruct: function () {},
                members: {
                    lblCurrent: null,
                    lblCurrentCry: null,
                    lblCurrentPow: null,
                    txtCurrentNewLevel: null,
                    Current: null,
                    lblAllCry: null,
                    lblAllPow: null,
                    txtAllNewLevel: null,
                    onAppear: function () {
                        var allLowLevel = this.calcAllLowLevel();
                        this.txtAllNewLevel.setValue(allLowLevel);
                        this.txtAllNewLevel.setMinimum(allLowLevel);
                        this.onAllNewLevelInput();
                    },
                    onClose: function () {
                        var qxApp = qx.core.Init.getApplication();
                        this.lblCurrent.setValue(qxApp.tr("Selected army unit:") + " -");
                        this.txtCurrentNewLevel.setValue(1);
                        this.txtCurrentNewLevel.resetValue();
                        this.Current = null;
                        this.txtAllNewLevel.setMinimum(1);
                        this.txtAllNewLevel.resetValue();
                    },
                    onSelectionChange: function(oldObj, newObj) {
                        var qxApp = qx.core.Init.getApplication();
                        if (newObj != null && newObj.get_VisObjectType() == ClientLib.Vis.VisObject.EObjectType.ArmyUnitType) {
                            this.Current = newObj;
                            var name = newObj.get_UnitName();
                            var level = newObj.get_UnitLevel();
                            this.lblCurrent.setValue(qxApp.tr("Selected army unit:") + " " + name + " (" + level.toString() + ")");
                            this.txtCurrentNewLevel.setMinimum(level + 1);
                            this.txtCurrentNewLevel.setValue(level + 1);
                            this.onCurrentNewLevelInput();
                        }
                    },
                    onCurrentNewLevelInput: function () {
                        var newLevel = parseInt(this.txtCurrentNewLevel.getValue(), 10);
                        if (newLevel > 0) {
                            var obj = this.Current;
                            if (obj !== null && obj.get_VisObjectType() == ClientLib.Vis.VisObject.EObjectType.ArmyUnitType && newLevel > obj.get_UnitLevel()) {
                                var costs = ClientLib.API.Army.GetInstance().GetUpgradeCostsForUnitToLevel(obj.get_UnitDetails(), newLevel);
                                var Cry = 0;
                                var Pow = 0;
                                for (var i = 0; i < costs.length; i++) {
                                    var uCosts = costs[i];
                                    var cType = parseInt(uCosts.Type, 10);
                                    switch (cType) {
                                        case ClientLib.Base.EResourceType.Crystal:
                                            Cry += uCosts.Count;
                                            break;
                                        case ClientLib.Base.EResourceType.Power:
                                            Pow += uCosts.Count;
                                            break;
                                    }
                                }
                                this.lblCurrentCry.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Cry));
                                this.lblCurrentCry.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Cry));
                                this.lblCurrentCry.setToolTipIcon("webfrontend/ui/common/icn_res_chrystal.png");
                                if (Cry === 0) this.lblCurrentCry.exclude();
                                else this.lblCurrentCry.show();
                                this.lblCurrentPow.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Pow));
                                this.lblCurrentPow.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Pow));
                                this.lblCurrentPow.setToolTipIcon("webfrontend/ui/common/icn_res_power.png");
                                if (Pow === 0) this.lblCurrentPow.exclude();
                                else this.lblCurrentPow.show();
                            } else {
                                this.lblCurrentCry.setLabel("-");
                                this.lblCurrentCry.resetToolTipText();
                                this.lblCurrentCry.resetToolTipIcon();
                                this.lblCurrentCry.show();
                                this.lblCurrentPow.setLabel("-");
                                this.lblCurrentPow.resetToolTipText();
                                this.lblCurrentPow.resetToolTipIcon();
                                this.lblCurrentPow.show();
                            }
                        } else {
                            this.lblCurrentCry.setLabel("-");
                            this.lblCurrentCry.resetToolTipText();
                            this.lblCurrentCry.resetToolTipIcon();
                            this.lblCurrentCry.show();
                            this.lblCurrentPow.setLabel("-");
                            this.lblCurrentPow.resetToolTipText();
                            this.lblCurrentPow.resetToolTipIcon();
                            this.lblCurrentPow.show();
                        }
                    },
                    upgradeCurrent: function() {
                        var newLevel = parseInt(this.txtCurrentNewLevel.getValue(), 10);
                        if (newLevel > 0) {
                            var obj = this.Current;
                            if (obj !== null && obj.get_VisObjectType() == ClientLib.Vis.VisObject.EObjectType.ArmyUnitType)
                                ClientLib.API.Army.GetInstance().UpgradeUnitToLevel(obj.get_UnitDetails(), newLevel);
                            this.onSelectionChange(null, this.Current);
                        }
                        this.txtCurrentNewLevel.setValue("");
                    },
                    calcAllLowLevel: function () {
                        for (var newLevel = 1, Cry = 0, Pow = 0; Cry === 0 && Pow === 0; newLevel++) {
                            var costs = ClientLib.API.Army.GetInstance().GetUpgradeCostsForAllUnitsToLevel(newLevel);
                            if (costs !== null) {
                                for (var i = 0; i < costs.length; i++) {
                                    var uCosts = costs[i];
                                    var cType = parseInt(uCosts.Type, 10);
                                    switch (cType) {
                                        case ClientLib.Base.EResourceType.Crystal:
                                            Cry += uCosts.Count;
                                            break;
                                        case ClientLib.Base.EResourceType.Power:
                                            Pow += uCosts.Count;
                                            break;
                                    }
                                }
                            }
                        }
                        return (newLevel - 1);
                    },
                    onAllNewLevelInput: function () {
                        var newLevel = parseInt(this.txtAllNewLevel.getValue(), 10);
                        var costs = ClientLib.API.Army.GetInstance().GetUpgradeCostsForAllUnitsToLevel(newLevel);
                        if (newLevel > 0 && costs !== null) {
                            var Cry = 0;
                            var Pow = 0;
                            for (var i = 0; i < costs.length; i++) {
                                var uCosts = costs[i];
                                var cType = parseInt(uCosts.Type, 10);
                                switch (cType) {
                                    case ClientLib.Base.EResourceType.Crystal:
                                        Cry += uCosts.Count;
                                        break;
                                    case ClientLib.Base.EResourceType.Power:
                                        Pow += uCosts.Count;
                                        break;
                                }
                            }
                            this.lblAllCry.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Cry));
                            this.lblAllCry.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Cry));
                            this.lblAllCry.setToolTipIcon("webfrontend/ui/common/icn_res_chrystal.png");
                            if (Cry === 0) this.lblAllCry.exclude();
                            else this.lblAllCry.show();
                            this.lblAllPow.setLabel(phe.cnc.gui.util.Numbers.formatNumbersCompact(Pow));
                            this.lblAllPow.setToolTipText(phe.cnc.gui.util.Numbers.formatNumbers(Pow));
                            this.lblAllPow.setToolTipIcon("webfrontend/ui/common/icn_res_power.png");
                            if (Pow === 0) this.lblAllPow.exclude();
                            else this.lblAllPow.show();
                        } else {
                            this.lblAllCry.setLabel("-");
                            this.lblAllCry.resetToolTipText();
                            this.lblAllCry.resetToolTipIcon();
                            this.lblAllCry.show();
                            this.lblAllPow.setLabel("-");
                            this.lblAllPow.resetToolTipText();
                            this.lblAllPow.resetToolTipIcon();
                            this.lblAllPow.show();
                        }
                    },
                    upgradeAll: function() {
                        var newLevel = parseInt(this.txtAllNewLevel.getValue(), 10);
                        if (newLevel > 0) ClientLib.API.Army.GetInstance().UpgradeAllUnitsToLevel(newLevel);
                        this.txtAllNewLevel.setValue("");
                    }
                }
            });
        }
        function translation() {
            var localeManager = qx.locale.Manager.getInstance();

            // Default language is english (en)
            // Available Languages are: ar,ce,cs,da,de,en,es,fi,fr,hu,id,it,nb,nl,pl,pt,ro,ru,sk,sv,ta,tr,uk
            // You can send me translations so i can include them in the Script.

            // German (incomplete)
            localeManager.addTranslation("de", {
                "Upgrade current building": "Markiertes Gebäude aufwerten",
                "Selected building:": "Markiertes Gebäude:",
                "Upgrade all buildings": "Alle Gebäude aufwerten",
                "Upgrade current defense unit": "Markierte Abwehrstellung aufwerten",
                "Selected defense unit:": "Markierte Abwehrstellung:",
                "Upgrade all defense units": "Alle Abwehrstellungen aufwerten",
                "Upgrade current army unit": "Markierte Armee-Einheit aufwerten",
                "Selected army unit:": "Markierte Armee-Einheit:",
                "Upgrade all army units": "Alle Armee-Einheiten aufwerten"
            });
        }
        function waitForGame() {
            try {
                if (typeof qx != 'undefined' && typeof qx.core != 'undfined' && typeof qx.core.Init != 'undefined') {
                    var app = qx.core.Init.getApplication();
                    if (app.initDone == true) {
                        try {
                            console.log("WarChiefs - Tiberium Alliances Upgrade Base/Defense/Army: Loading");
                            translation();
                            createClasses();
                            Upgrade.getInstance();
                            Upgrade.Base.getInstance();
                            Upgrade.Defense.getInstance();
                            Upgrade.Army.getInstance();
                            console.log("WarChiefs - Tiberium Alliances Upgrade Base/Defense/Army: Loaded");
                        } catch (e) {
                            console.log(e);
                        }
                    } else {
                        window.setTimeout(waitForGame, 1000);
                    }
                } else {
                    window.setTimeout(waitForGame, 1000);
                }
            } catch (e) {
                console.log(e);
            }
        }
        window.setTimeout(waitForGame, 1000);
    };

    var script = document.createElement("script");
    var txt = injectFunction.toString();
    script.innerHTML = "(" + txt + ")();";
    script.type = "text/javascript";

    document.getElementsByTagName("head")[0].appendChild(script);
})();